/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services.impl;

import cl.bci.propuesta.TransactionException;
import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.services.DatabaseFacade;
import cl.bci.propuesta.services.TransactionValidator;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author evasquez
 */
@Service
public class TransactionValidatorImpl implements TransactionValidator{
    
    @Autowired
    private DatabaseFacade dbfacade;
    
    private static final Logger LOG = LoggerFactory.getLogger(TransactionValidatorImpl.class);
    
    private final Pattern emailValidatorPattern;
    private final Pattern passwordValidatorPattern;
    
    public TransactionValidatorImpl(){
        // this validates emails with the form: aaaaa@dominio.cl
        // THIS IS NOT VALIDATING A REAL EMAIL which is far more complex
        emailValidatorPattern=Pattern.compile("^[a-z]+@[a-z]+\\.[a-z]+$");
        // created in https://regex101.com/
        passwordValidatorPattern=Pattern.compile("^(?=(.*\\d){2})(?=.*[a-z]{1})(?=.*[A-Z]{1})[0-9a-zA-Z]{4,}$");
    }


    @Override
    public void validate(UserVO user) {
        LOG.debug("Validating user "+user.getEmail());
        validateEmail(user);
        validatePassword(user);
        LOG.debug("User "+user.getEmail()+" is valid");
    }

    @Override
    public void validateForCreation(UserVO user) {
        LOG.debug("Validating user for creation: "+user.getEmail());
        // look for the email in database
        validate(user);
        Boolean found=dbfacade.findEmailExists(user.getEmail());
        LOG.info("User "+user.getEmail()+" is "+(found?"already":"not")+" in database");
        if(found) throw new TransactionException("error.email.registered",user.getEmail());
    }
    
    /**
     * Validates an email according specification<br/>
     * THIS IS NOT VALIDATING A VALID EMAIL, just the documnetation pattern: "aaaaaa@dominio.cl"
     * @param user Received User Object from user
     */
    public void validateEmail(UserVO user){
        String email=user.getEmail();
        LOG.debug("Validating "+email);
        if(!emailValidatorPattern.matcher(email).matches()){
            LOG.debug("Email "+email+" is not valid");
            throw new TransactionException("error.email.format", email);
        }
        LOG.debug("Email "+email+" is valid");
    }
    
    /**
     * Validates the password according spec<br/>
     * @param user Received User Object from user
     */
    public void validatePassword(UserVO user){
        String password=user.getPassword();
        LOG.debug("Validating password for user "+user.getEmail());
        if(password!=null && !passwordValidatorPattern.matcher(password).matches()){
            LOG.debug("Password for user "+user.getEmail()+" is not strong enough");
            throw new TransactionException("error.password.weak");
        }
        LOG.debug("Good password for user "+user.getEmail());
    }
    
}
