/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services;

import cl.bci.propuesta.controller.vo.UserVO;
import java.util.UUID;

/**
 *
 * @author evasquez
 */
public interface DatabaseFacade {
    public UserVO getUser(UUID id);
    public UserVO saveUser(UserVO user);
    public Boolean findEmailExists(String email);
}
