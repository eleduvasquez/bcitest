/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services.impl;

import cl.bci.propuesta.TransactionException;
import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.persistence.UserRepository;
import cl.bci.propuesta.persistence.entitites.User;
import cl.bci.propuesta.services.Comparator;
import cl.bci.propuesta.services.Converter;
import cl.bci.propuesta.services.DatabaseFacade;
import java.util.Date;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author evasquez
 */
@Repository
public class DatabaseFacadeImpl implements DatabaseFacade{

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseFacade.class);
    
    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private Converter converter;
    
    @Autowired
    private Comparator comparator;
    
    @Override
    public UserVO getUser(UUID id) {
        LOG.debug("Looking for user with UUID {0}",(id!=null?id.toString():"NULL"));
        if(id==null) throw new TransactionException("error.uuid.null");
        return converter.convert(userRepo.findById(id).orElseThrow(()->new TransactionException("error.user.id.notfound",id.toString())));
    }

    @Override
    public UserVO saveUser(UserVO user) {
        LOG.debug("Saving user : "+user.getEmail());
        User toDB=converter.convert(user);
        if(toDB.getId()!=null){
            LOG.debug("User "+user.getEmail()+" has ID");
            User fromDB = userRepo.findById(user.getId()).orElseThrow(() -> new TransactionException("error.user.id.notfound",toDB.getId().toString()));
            LOG.debug("User "+user.getEmail()+" found in DB with UUID "+user.getId());
            user.setCreated(fromDB.getCreated());
            if(user.getIsactive()==null) user.setIsactive(fromDB.getIsActive()); // if no isActive specified, then get from db
            if(user.getLast_login()==null) user.setLast_login(fromDB.getLast_login());
            user.setModified(new Date());
        } else {
            LOG.debug("New User "+user.getEmail());
            Date timestamp=new Date();
            toDB.setIsActive(Boolean.TRUE); // not sure if this should start as active
            toDB.setCreated(timestamp);
            toDB.setModified(timestamp);
            toDB.setLast_login(timestamp);
        }
        User fromDB=userRepo.saveAndFlush(toDB);
        UserVO ret=converter.convert(fromDB);
        LOG.debug("Removing password from response");
        ret.setPassword(null);
        return ret;
    }

    @Override
    public Boolean findEmailExists(String email) {
        LOG.debug("Looking if the user's email "+email+" is already in use");
        boolean inUse=!userRepo.findByEmail(email).isEmpty();
        LOG.debug("The email "+email+" was "+(inUse?"found":"not found")+" in database");
        return inUse;
    }
    
}
