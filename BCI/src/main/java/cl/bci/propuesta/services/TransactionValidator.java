/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services;

import cl.bci.propuesta.controller.vo.UserVO;

/**
 *
 * @author evasquez
 */
public interface TransactionValidator {
     /**
     * Validates an user by specification requirements: only e-mail
     * @param user 
     */
    public void validate(UserVO user);
    /**
     * Looks for the email in database
     * @param user User to create
     */
    public void validateForCreation(UserVO user);
}
