/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services.impl;

import cl.bci.propuesta.controller.vo.PhoneVO;
import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.persistence.entitites.Phone;
import cl.bci.propuesta.persistence.entitites.User;
import cl.bci.propuesta.services.Comparator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author evasquez
 */
@Service
public class ComparatorImpl implements Comparator{

    /**
     * Transaction object will be equal not comparing the ID, creation and other stuff</br>
     * because they come from User
     * @param _this one element
     * @param _other the other element
     * @return true if they refer to the same object
     */
    @Override
    public Boolean compare(UserVO _this, UserVO _other) {
        if(bothNull(_this,_other)) return true;
        if(xorNull(_this,_other)) return false;
        Boolean equal=true;
        equal &= equalsImpl(_this.getEmail(), _other.getEmail());
        equal &= equalsImpl(_this.getName(),_other.getName());
        equal &= equalsImpl(_this.getPhones(),_other.getPhones());
        return equal;
    }

    /**
     * This compares two elements which refers to the same object <br/>
     * This is intended to detect when to update modify
     * @param _this one element
     * @param _other the other element
     * @return true if they have the same core data, false otherwise
     */
    @Override
    public Boolean compare(User _this, User _other) {
        if(bothNull(_this,_other)) return true;
        if(xorNull(_this, _other)) return false;
        Boolean equal=true;
        equal &= equalsImpl(_this.getEmail(), _other.getEmail());
        equal &= equalsImpl(_this.getName(), _other.getName());
        equal &= equalsImpl(_this.getPassword(), _other.getPassword());
        equal &= equalsImpl(_this.getPhones(), _other.getPhones());
        return equal;
    }

    @Override
    public Boolean compare(PhoneVO _this, PhoneVO _other) {
        if(bothNull(_this,_other)) return true;
        if(xorNull(_this,_other)) return false;
        Boolean equal=true;
        equal &= equalsImpl(_this.getCitycode(), _other.getCitycode());
        equal &= equalsImpl(_this.getContrycode(), _other.getContrycode()); // Requirement says county on JSON request
        equal &= equalsImpl(_this.getNumber(), _other.getNumber());
        return equal;
    }

    @Override
    public Boolean compare(Phone _this, Phone _other) {
        if(bothNull(_this,_other)) return true;
        if(xorNull(_this,_other)) return false;
        Boolean equal=true;
        equal &= equalsImpl(_this.getCitycode(), _other.getCitycode());
        equal &= equalsImpl(_this.getCountrycode(), _other.getCountrycode());
        equal &= equalsImpl(_this.getNumber(), _other.getNumber());
        return equal;
    }
    
    private Boolean bothNull(Object a, Object b){
        return (a==null && b==null);
    }
    
    private Boolean xorNull(Object a, Object b){
        return ((a==null && b!=null) || (a!=null && b==null));
    }
    
    private Boolean equalsImpl(Object a, Object b){
        boolean ret=bothNull(a, b) || (!xorNull(a, b) && a.equals(b));
        if(ret){
            if(a instanceof Collection && b instanceof Collection){
              List la=new ArrayList((Collection)a);  // probably there's a better list for this
              List lb=new ArrayList((Collection)b);  // probably there's a better list for this
              if(la.size()!=lb.size()) return false;
              for(int i=0;i<la.size();i++){
                  Object ia=la.get(i);
                  Object ib=lb.get(i);
                  if(ia instanceof PhoneVO && !compare((PhoneVO)ia, (PhoneVO)ib)) 
                        ret=false;
                  if(ia instanceof Phone && !compare((Phone)ia, (Phone)ib))
                        ret=false;
                  if(!ret) break;
              }
            }
        }
        return ret;
        
    }
    
}
