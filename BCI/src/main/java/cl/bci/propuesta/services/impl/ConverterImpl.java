/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services.impl;

import cl.bci.propuesta.controller.vo.PhoneVO;
import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.persistence.entitites.Phone;
import cl.bci.propuesta.persistence.entitites.User;
import cl.bci.propuesta.services.Converter;
import org.springframework.stereotype.Service;

/**
 *
 * @author evasquez
 */
@Service
public class ConverterImpl implements Converter{

    @Override
    public UserVO convert(User user) {
        UserVO dest=new UserVO();
        dest.setId(user.getId());
        dest.setCreated(user.getCreated());
        dest.setIsactive(user.getIsActive());
        dest.setLast_login(user.getLast_login());
        dest.setModified(user.getModified());
        dest.setEmail(user.getEmail());
        dest.setName(user.getName());
        dest.setPassword(user.getPassword());
        dest.setToken(user.getToken());
        user.getPhones().stream().forEach(phone->{
            PhoneVO phoneto=new PhoneVO();
            phoneto.setCitycode(phone.getCitycode());
            phoneto.setContrycode(phone.getCountrycode()); // Requirement says county instead of country in JSON request
            phoneto.setId(phone.getId());
            phoneto.setNumber(phone.getNumber());
            dest.addPhone(phoneto);
        });
        return dest;
    }

    @Override
    public User convert(UserVO user) {
        User dest=new User();
        dest.setEmail(user.getEmail());
        dest.setName(user.getName());
        dest.setPassword(user.getPassword());
        dest.setToken(user.getToken());
        user.getPhones().stream().forEach(phoneto->{
            Phone phone=new Phone();
            phone.setCitycode(phoneto.getCitycode());
            phone.setCountrycode(phoneto.getContrycode()); // Requirement says county instead of Country in JSON request
            phone.setNumber(phoneto.getNumber());
            phone.setUser(dest);
            dest.getPhones().add(phone);
        });
        return dest;
    }
    
    
    
}
