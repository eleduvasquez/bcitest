/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services;

import cl.bci.propuesta.controller.vo.PhoneVO;
import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.persistence.entitites.Phone;
import cl.bci.propuesta.persistence.entitites.User;

/**
 *
 * @author evasquez
 */
public interface Comparator {
    public Boolean compare(UserVO _this, UserVO _other);
    public Boolean compare(User _this, User _other);
    public Boolean compare(PhoneVO _this, PhoneVO _other);
    public Boolean compare(Phone _this, Phone _other);
}
