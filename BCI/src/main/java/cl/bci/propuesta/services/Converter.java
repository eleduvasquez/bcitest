/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.services;

import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.persistence.entitites.User;

/**
 *
 * @author evasquez
 */
public interface Converter {
    UserVO convert(User user);    
    User convert(UserVO user);
}
