/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.controller.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author evasquez
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PhoneVO implements TransactionObject {
    private UUID id;
    private String number;
    private String citycode; // this should be citiyCode by camelCase good practice
    private String countrycode; // this should be countryCode by camelCase good practice
    
    public PhoneVO(){
        
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public String getContrycode() { // Requirment says county instead of Country
        return countrycode;
    }

    public void setContrycode(String countrycode) { // Requirement says county instead of Country
        this.countrycode = countrycode;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.id);
        hash = 11 * hash + Objects.hashCode(this.number);
        hash = 11 * hash + Objects.hashCode(this.citycode);
        hash = 11 * hash + Objects.hashCode(this.countrycode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PhoneVO other = (PhoneVO) obj;
        if (!Objects.equals(this.number, other.number)) {
            return false;
        }
        if (!Objects.equals(this.citycode, other.citycode)) {
            return false;
        }
        if (!Objects.equals(this.countrycode, other.countrycode)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }


    
    
}
