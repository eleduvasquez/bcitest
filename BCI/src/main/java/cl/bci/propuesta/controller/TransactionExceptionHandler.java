/* https://www.toptal.com/java/spring-boot-rest-api-error-handling */
package cl.bci.propuesta.controller;

import cl.bci.propuesta.TransactionException;
import cl.bci.propuesta.config.TomcatConfig;
import cl.bci.propuesta.controller.vo.TransactionResponse;
import java.text.MessageFormat;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * This handles the controlled exceptions raised
 * @author evasquez
 */
@RestControllerAdvice
public class TransactionExceptionHandler {
    
    private static final Logger LOG = LoggerFactory.getLogger(TransactionExceptionHandler.class);
    
    @Autowired
    private MessageSource messageSource;
    
    /**
     * Format the error with internationalization
     * @param ex Exception to process
     * @param locale User's locale
     * @return TransactionResponse with an error message
     */
    @ExceptionHandler(TransactionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public TransactionResponse badRequest(Exception ex, Locale locale){
        LOG.warn("Error reported "+ex.getMessage());
        TransactionResponse response=new TransactionResponse();
        response.setIsError(true);
        String message=messageSource.getMessage(ex.getMessage(), null, locale);
        response.setMessage(message);
        if(ex instanceof TransactionException){
            TransactionException tex=(TransactionException) ex;
            String[] references=tex.getReference();
            message=messageSource.getMessage(ex.getMessage(), references, locale);
            response.setMessage(message);
        }
        return response;
    }
    
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public TransactionResponse emptyBody(Exception ex, Locale locale){
        LOG.warn("Error reported: No body content in request");
        TransactionResponse response=new TransactionResponse();
        response.setIsError(true);
        response.setMessage(messageSource.getMessage("error.body.empty", null, locale));
        return response;
    }
}
