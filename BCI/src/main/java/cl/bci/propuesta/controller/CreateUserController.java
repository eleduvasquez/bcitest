/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.controller;

import cl.bci.propuesta.config.TomcatConfig;
import cl.bci.propuesta.controller.vo.UserVO;
import cl.bci.propuesta.services.DatabaseFacade;
import cl.bci.propuesta.services.TransactionValidator;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author evasquez
 */
@RestController
public class CreateUserController {
    
    private static final Logger LOG = LoggerFactory.getLogger(CreateUserController.class);
    
    @Autowired
    private TransactionValidator validator;
    
    @Autowired
    private DatabaseFacade dbfacade;
    
    private final Long jwtLifespan;
    private String jwtSecret;
    
    public CreateUserController(@Value("${jwt.lifespan.ms}")Long jwtLifespan, @Value("jwt.secret")String jwtSecret){
        this.jwtLifespan=jwtLifespan;
        this.jwtSecret=jwtSecret;
    }
    
    /**
     * Add user
     * @param user User to add
     * @return Transaction response with UUID and user from database with no exposing password
     */
    @RequestMapping("/adduser")
    private ResponseEntity<UserVO> addUser(@RequestBody UserVO user){
        LOG.info("Adding user "+user.getEmail());
        validator.validateForCreation(user);
        LOG.info("User "+user.getEmail()+" email is valid");
        populateToken(user);
        return new ResponseEntity<>(dbfacade.saveUser(user), HttpStatus.OK);
    }
    
    private void populateToken(UserVO user){
        if(user.getToken()==null || !validateToken(user)){
            LOG.info("Creating token for "+user.getEmail());
            // https://auth0.com/blog/implementing-jwt-authentication-on-spring-boot/
            String jwt=JWT.create()
                    .withSubject(user.getName())
                    .withExpiresAt(new Date(System.currentTimeMillis()+jwtLifespan))
                    .sign(Algorithm.HMAC512(jwtSecret.getBytes()));
            user.setToken(jwt);
            LOG.info("Token for "+user.getEmail()+" created: "+jwt);
        }
    }
    
    private Boolean validateToken(UserVO user){
        LOG.info("Validating token for "+user.getEmail());
        try{
            String token=user.getToken();
            JWTVerifier verifier=JWT.require(Algorithm.HMAC512(jwtSecret)).withIssuer("auth0").build();
            DecodedJWT jwt_decoded = verifier.verify(token);
        } catch (JWTVerificationException ex){
            LOG.info("Invalid token for "+user.getEmail());
            return false;
        }
        LOG.info("Valid token for "+user.getEmail());
        return true;
    }
            
}
