/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.controller.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author evasquez
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TransactionResponse {
    private TransactionObject payload;
    
    private String message;
    private boolean isError;
    
    public TransactionResponse(){    
    }

    public TransactionObject getPayload() {
        return payload;
    }

    public void setTransaction(TransactionObject payload) {
        this.payload = payload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIsError() {
        return isError;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }
    
    
}
