/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta;

import java.util.Arrays;

/**
 *
 * @author evasquez
 */
public class TransactionException extends RuntimeException{
    
    private String[] reference;
    
    public TransactionException(){
        super();
    }
    
    public TransactionException(String msg){
        super(msg);
    }
    
    public TransactionException(String... reference){
        this(reference[0]);
        this.reference=Arrays.asList(reference).subList(1, reference.length).toArray(new String[reference.length-1]);
    }

    public String[] getReference(){
        return reference;
    }
}
