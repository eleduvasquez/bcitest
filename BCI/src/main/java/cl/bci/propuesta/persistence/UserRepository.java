/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bci.propuesta.persistence;

import cl.bci.propuesta.persistence.entitites.User;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author evasquez
 */
@Repository
public interface UserRepository extends JpaRepository<User, UUID>{
    public List<User> findByEmail(String email);
}
